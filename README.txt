
About yast theme
--------------------
Drupal 6 yet another social theme
Created by Carlos Espino


Yast is a Framework subtheme. Yast is a facebook look theme, clean and minimalist.

Features
========

  Yast features:
  ================
  	* Sidebars can be placed on several modes: only left bar, only right bar, left and right bars, two bars on the left or two bars on the right. When two sidebars are in the same side, two new bars, called "double top" and "double banner", are over xxxxx
  	* Sidebars and container width can be customized.
  	* Color schemas:
    	- 17 colors schemas, easily customizables. 
    	- When you config the theme, you can display a selector and yours users can select persistent custom schema (with cookies)
    * Included in Double Top region a "yaSt logo" schema dependent. You must edit the logos under schemas/images folder.
  	* Support for:
  	  - logo
  	  - name
  	  - slogan
  	  - mission
  	  - node_user_picture
  	  - comment_user_picture
  	  - search
  	  - favicon
  	  - secondary links
  	  - primary links (every time actives in facebook style nav bar)
  	 *Regions:
  	  - Banner top
  	  - Left sidebar
  	  - Right sidebar
  	  - Header
  	  - Footer
  	  - Content
  	  - Double top
  	  - Double banner 
	  * Suckerfish primary links menu (dropdowns menus)
 		
  Framework features:
  ===================
 		* Framework gives general placement and formatting to basic Drupal elements
		* Supports one, two, and three-column layouts
		* Set to a 24 column grid of 950px
		* CSS file is highly organized, including a table of contents, section flags, alphabetical properties, etc.
		* Includes a CSS reset and a list of CSS utility classes for easy content formatting
		* Em unit text sizing with vertical rhythm
		* Search in sidebar (as a block) and header (as a theme configuration option)
		* Included support for Dynamic Persistent Menu
		* Quick block and view editing links
		* Clean and simplified code, file structure, and administration section
		* Works nicely in mobile browsers
		* W3C valid CSS 2.1 / XHTML 1.1
		* Verified and tested with Firefox 3, Firefox 2, IE7, IE6, Safari 4, Chrome
		
		
>>> INSTALL to sites/all/themes


>>> CONFIGURATION NOTES:

To add regions, see: http://drupal.org/node/242107#comment-798428

To enable current node to show in the breadcrumb trail, remove comment slashes on line 41 of template.php in framework folder


>>> SUPPORT

If you have questions or problems, check the issue list before submitting a new issue: 
http://drupal.org/project/issues/yast

For general support, please refer to:
http://drupal.org/support

To contact me directly, please visit: 
http://drupal.org/user/96976/contact
