<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" 
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" >
  <head>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    
    <?php 
      if ( theme_get_setting('social_schema') != 'yast.css' ){
        $custom_schema = theme_get_setting('social_schema');
        if (theme_get_setting('social_schemas_selector'))
          if ( isset( $_COOKIE['yast_schema'] ) )
            $custom_schema = $_COOKIE['yast_schema'];
        $full_custom_schema = base_path() . path_to_theme() . '/schemas/' . $custom_schema ;
        print "<link type=\"text/css\" rel=\"stylesheet\" media=\"all\" href=\"$full_custom_schema\" />";
      }
    ?>
    
   
    <?php print $scripts ?>
    <!--[if lte IE 7]><?php print framework_get_ie_styles(); ?><![endif]--> <!--If Less Than or Equal (lte) to IE 7-->
    <?php print make_aditional_styles($left,$right);  ?>
  </head>


  <body<?php print framework_body_class($left, $right); ?>>

    <!-- Banner -->
    <div id="banner_top">
      <?php print $banner_top; ?>
    </div> <!-- /Banner -->
    

    <!-- Logo, site name and navigation -->
    <div id="nav">

      <?php if ($logo): ?>
        <div id="logobox">
          <a href="<?php print check_url($front_page); ?>" title="<?php print check_plain($site_name); ?>">
            <img src="<?php print check_url($logo); ?>" alt="<?php print check_plain($site_name); ?>" id="logo" />
          </a>
        </div>
      <?php endif; ?>

      <div id="sitename">
        <?php if ($site_name): ?>
          <h1><a href="<?php print check_url($front_page); ?>" title="<?php print check_plain($site_name); ?>"><?php print check_plain($site_name); ?></a></h1>
        <?php endif; ?>
      </div> <!-- /#sitename -->

        <?php if ($site_slogan): ?>
            <div id="siteslogan"><?php print check_plain($site_slogan); ?></div>
        <?php endif; ?>

      <?php if (theme_get_setting('social_schemas_selector')): ?>
        <div id="schemas_selector">
          <?php print drupal_get_form('social_schemas_selector'); ?>
        </div>
        <div id="schemas_label"><?php print t("Schema select") ?></div>
      <?php endif; ?>

      <div id="searchbox">
        <?php if ($search_box): ?><?php print $search_box ?><?php endif; ?>
      </div>

      <div id="social_menu">
        <? print menu_tree($menu_name = 'primary-links'); ?>
      </div>

    </div> <!-- /#nav -->

    <!-- Layout -->
    <div class="container" >

      <?php print get_side('left', $left, $right, $double_top, $double_banner, check_url($front_page), check_plain($site_name)); ?>


      <div id=container_main>
        <div id="main_social">
          <?php print $breadcrumb; ?>
          <?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>

         	<div id="header" class="clearfix">
          	<?php print $header; ?>
        	</div> <!-- /#header -->

          <?php if ($title): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
          <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block"><ul class="tabs primary">'. $tabs .'</ul>'; endif; ?>
          <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
          <?php if ($tabs): print '<span class="clear"></span></div>'; endif; ?>
          <?php if ($show_messages && $messages): print $messages; endif; ?>
          <?php print $help; ?>
          <?php print $content ?>
        </div> <!-- /#main_social -->
      </div><!-- /#container_main -->


      <?php print get_side('right', $left, $right, $double_top, $double_banner, check_url($front_page), check_plain($site_name)); ?>


      <div id="footer" class="clear">
        <?php print $footer_message . $footer ?>
        <?php print $feed_icons ?>
      </div> <!-- /#footer -->

    </div> <!-- /.container -->
    <!-- /layout -->
    
    <!-- Secondary links  -->
    <?php if ($secondary_links): ?>
      <div id="secondary" class="clear-block">
      <?php print theme('links', $secondary_links); ?>
      </div> <!-- /#secondary -->
    <?php endif; ?>
    <!-- /Secondary links  -->

  <?php print $closure ?>

  </body>
</html>
