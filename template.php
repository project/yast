<?php
/*
* Initialize theme settings
*/
if (is_null(theme_get_setting('social_sidebar_left_width'))) {  // <-- change this line
  global $theme_key;

  /*
   * The default values for the theme variables. Make sure $defaults exactly
   * matches the $defaults in the theme-settings.php file.
   */
  $defaults = array(             // <-- change this array
    'social_sidebar_left_width' => "180px",
    'social_sidebar_right_width' => "180px",
    'social_content_width' => "1024px",
    'social_bars_position' => "left_right",
    'social_schema' => "yast.css",
    'social_schemas_selector' => true,
  );

  // Get default theme settings.
  $settings = theme_get_settings($theme_key);
  // Don't save the toggle_node_info_ variables.
  if (module_exists('node')) {
    foreach (node_get_types() as $type => $name) {
      unset($settings['toggle_node_info_' . $type]);
    }
  }
  // Save default theme settings.
  variable_set(
    str_replace('/', '_', 'theme_'. $theme_key .'_settings'),
    array_merge($defaults, $settings)
  );
  // Force refresh of Drupal internals.
  theme_get_setting('', TRUE);
}

function social_schemas_selector_submit($form_id, $form_values){
  setcookie('yast_schema', $form_values['values']['social_schemas'], time() + 3600 * 24 );
}


function social_schemas_selector(){
  $schemas = array(
    'yast.css'=> t("Default"),
    'yast.bluelagon.css'=> t("Blue Lagoon"),
    'yast.ash.css'=> t("Ash"),
    'yast.aquamarine.css'=> t("Aquamarine"),
    'yast.chocolate.css'=> t("Belgian Chocolate"),
    'yast.bluemarine.css'=> t("Bluemarine"),
    'yast.citrus.css'=> t("Citrus Blast"),
    'yast.coldday.css'=> t("Cold Day"),
    'yast.greenbeam.css'=> t("Greenbeam"),
    'yast.mediterrano.css'=> t("Mediterrano"),
    'yast.mercury.css'=> t("Mercury"),
    'yast.nocturnal.css'=> t("Nocturnal"),
    'yast.olivia.css'=> t("Olivia"),
    'yast.pink.css'=> t("Pink Plastic"),
    'yast.tomato.css'=> t("Shiny Tomato"),
    'yast.teal.css'=> t("Teal Top"),
    'yast.red.css'=> t("Red"),
  );
  
  $default_schema = theme_get_setting('social_schema');
  if ( isset( $_COOKIE['yast_schema'] ) )
          $default_schema = $_COOKIE['yast_schema'];
          
  $form['social_schemas'] = array(
    '#type' => 'select',
    '#default_value' => $default_schema,
    '#options' => $schemas, 
    '#attributes' => array(
      'onchange' => 'submit()'),
    )  ;
    $form['submit'] = array('#type' => 'submit', '#value' => t('Save'), '#attributes' => array('style'=>'display:none')  );
  return $form;
}

if (theme_get_setting('social_schemas_selector')){
  global $social_schemas_selector;
  $social_schemas_selector = drupal_get_form('social_schemas_selector');
  return $social_schemas_selector;

}



function make_aditional_styles($left,$right) {
	$social_sidebar_left_width = 0;
	$social_sidebar_right_width = 0;

	$social_bars_position = theme_get_setting('social_bars_position');

	$social_content_width = theme_get_setting('social_content_width');
	$social_content_width = str_replace("px", "", $social_content_width);
	$social_content_width = str_replace("%", "", $social_content_width);


	if ( $social_bars_position != 'right' ){
		$social_sidebar_left_width = theme_get_setting('social_sidebar_left_width');
		$social_sidebar_left_width = str_replace("px", "", $social_sidebar_left_width);
		$social_sidebar_left_width = str_replace("%", "", $social_sidebar_left_width);
	}

	if ( $social_bars_position != 'left' ){
		$social_sidebar_right_width = theme_get_setting('social_sidebar_right_width');
		$social_sidebar_right_width = str_replace("px", "", $social_sidebar_right_width);
		$social_sidebar_right_width = str_replace("%", "", $social_sidebar_right_width);
	}

  if ( empty($left) ){
    $social_sidebar_left_width = 0;
  }
  
  if ( empty($right) ){
    $social_sidebar_right_width = 0;
  }
  
  if ( $social_bars_position =='left_left' or $social_bars_position =='right_right' ){
    $social_sidebar_double_width = $social_sidebar_right_width + $social_sidebar_left_width;
  }

	$social_main_width = $social_content_width - $social_sidebar_left_width - $social_sidebar_right_width;
	
	return " 

		<style type=\"text/css\"> 

			.container {
			  width: ".$social_content_width."px;
			}

			#container_left {
			  width: ".$social_sidebar_left_width."px;
			  float: left;
			}	

			#container_right {
			  width: ".$social_sidebar_right_width."px;
			  float: left;
			}

			#container_main {
			 width: ".$social_main_width."px;
			 float: left;
			}
			
			#container_double {
			  width: ".$social_sidebar_double_width."px;
			  float: left;
			}
			
		</style>";
}

function make_social_logo($custom_link, $custom_title){
  $custom_schema = theme_get_setting('social_schema');
  if (theme_get_setting('social_schemas_selector'))
    if ( isset( $_COOKIE['yast_schema'] ) )
      $custom_schema = $_COOKIE['yast_schema'];
  $custom_image = str_replace('css','png',$custom_schema);
  $full_custom_image = base_path() . path_to_theme() . '/schemas/images/' . $custom_image ;
  return "<a title=\"$custom_title\" href=\"$custom_link\"><img style=\"margin-top:10px;width:100%\" src=\"$full_custom_image\"></a>";
}

function print_double( $left, $right, $double_top, $double_banner ){
  return "
              <div id=\"container_double\">
                <div id=\"double_top\">$double_top</div><!-- /double_top -->
                <div id=\"container_left\">
                  <div id=\"sidebar-left\" class=\"sidebar\">$left</div> <!-- /#sidebar-left -->
                </div> <!-- /#container-left -->
                <div id=\"container_right\">
                  <div id=\"sidebar-right\" class=\"sidebar\">$right</div> <!-- /#sidebar-right -->
                </div> <!-- /#container-right -->
                <div  class=\"clearfix\"></div>
                <div id=\"double_banner\">$double_banner</div><!-- /double_banner -->
              </div><!-- /container_double -->
                ";
}

function print_single_sidebar ($singlebar, $hand){
  return "
                <div id=\"container_$hand\">
                  <div id=\"sidebar-$hand\" class=\"sidebar\">$singlebar</div> <!-- /#sidebar-$hand -->
                </div> <!-- /#container-$hand -->
    ";
}

function get_side($side, $left, $right, $double_top, $double_banner, $custom_link, $custom_title){
  $social_bars_position = theme_get_setting('social_bars_position');

  switch ( theme_get_setting('social_logo') ){
    case 'left' ;
      $left = make_social_logo($custom_link, $custom_title) . $left;
      break;
    case 'right' ;
      $right = make_social_logo($custom_link, $custom_title) . $right;
      break;
    case 'double_top' ;
      $double_top = make_social_logo($custom_link, $custom_title) . $double_top;
      break;
    case 'double_banner' ;
      $double_banner = make_social_logo($custom_link, $custom_title) . $double_banner;
      break;
  }
  
  if ( $side == 'left' ) {
    if ( $social_bars_position == 'left_left' ){
      $side_content = print_double( $left, $right, $double_top, $double_banner );
    }
    if ( $social_bars_position == 'left' or $social_bars_position == 'left_right' ){
      $side_content = print_single_sidebar ($left, 'left');
    }
  }
  
  if ( $side == 'right' ) {
    if ( $social_bars_position == 'right_right' ){
      $side_content = print_double( $left, $right, $double_top, $double_banner );
    }
    if ( $social_bars_position == 'right' or $social_bars_position == 'left_right' ){
      $side_content = print_single_sidebar ($right, 'right');
    }
  }



  return $side_content;
}



?>
