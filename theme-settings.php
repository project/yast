<?php

/**
* Implementation of THEMEHOOK_settings() function.
*
* @param $saved_settings
*   array An array of saved settings for this theme.
* @return
*   array A form array.
*/
function phptemplate_settings($saved_settings) {
  /*
   * The default values for the theme variables. Make sure $defaults exactly
   * matches the $defaults in the template.php file.
   */

  $schemas = array(
    'yast.css'=> t("Default"),
    'yast.bluelagon.css'=> t("Blue Lagoon"),
    'yast.ash.css'=> t("Ash"),
    'yast.aquamarine.css'=> t("Aquamarine"),
    'yast.chocolate.css'=> t("Belgian Chocolate"),
    'yast.bluemarine.css'=> t("Bluemarine"),
    'yast.citrus.css'=> t("Citrus Blast"),
    'yast.coldday.css'=> t("Cold Day"),
    'yast.greenbeam.css'=> t("Greenbeam"),
    'yast.mediterrano.css'=> t("Mediterrano"),
    'yast.mercury.css'=> t("Mercury"),
    'yast.nocturnal.css'=> t("Nocturnal"),
    'yast.olivia.css'=> t("Olivia"),
    'yast.pink.css'=> t("Pink Plastic"),
    'yast.tomato.css'=> t("Shiny Tomato"),
    'yast.teal.css'=> t("Teal Top"),
    'yast.red.css'=> t("Red"),
  );

  $defaults = array(
    'social_sidebar_left_width' => "180px",
    'social_sidebar_right_width' => "180px",
    'social_content_width' => "1024px",
	  'social_bars_position' => "left_right",
	  'social_schema' => "yast.css",
	  'social_schemas' => $schemas,
	  'social_schemas_selector' => true,
  );

  // Merge the saved variables and their default values
  $settings = array_merge($defaults, $saved_settings);

  // Create the form widgets using Forms API
  $form['social_sidebar_left_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Left sidebar'),
    '#default_value' => $settings['social_sidebar_left_width'],
  );
  $form['social_sidebar_right_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Right sidebar'),
    '#default_value' => $settings['social_sidebar_right_width'],
  );
  
  $form['social_content_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Content'),
    '#default_value' => $settings['social_content_width'],
  );
  
  $form['social_bars_position'] = array(
    '#type' => 'select',
    '#title' => t('Sidebars position'),
    '#default_value' => $settings['social_bars_position'],
    '#options' => array( 'left_right' => t("Left and right"), 'left' => t("Left"), 'right' => t("Right"), 'left_left' => t("Two bars on the left"), 'right_right' => t("Two bars on the right")   )  ,
    '#description' => t('Select sidebars position')
  );


  $form['social_schema'] = array(
    '#type' => 'select',
    '#title' => t('Colors schema'),
    '#default_value' => $settings['social_schema'],
    '#options' => $schemas, 
    '#description' => t('Select colors schema')
  );
  
  $form['social_schemas_selector'] = array(
    '#type' => 'checkbox',
    '#title' => t('Colors schemas selector'),
    '#default_value' => $settings['social_schemas_selector'],
    '#description' => t('Select colors schema box in nav bar')
  );

  $form['social_logo'] = array(
    '#type' => 'select',
    '#title' => t('Bar logo'),
    '#options' => array( 'none' => t("None"), 'left' => t("Left"), 'right' => t("Right"), 'double_top' => t("Top double bar"), 'double_banner' => t("Double banner")   )  ,
    '#default_value' => $settings['social_logo'],
    '#description' => t('Display bar logo')
  );

  // Return the additional form widgets
  return $form;
}
?>
